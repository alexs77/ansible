```
alex@alex-kubuntu:~/Dokumente/Privat/ansible/setup-linux$ ansible-playbook -i inventory site.yml -v
Using /etc/ansible/ansible.cfg as config file

PLAY [all] *******************************************************************************************************************************

TASK [Gathering Facts] *******************************************************************************************************************
ok: [localhost]

TASK [repos : Manage key of repository] **************************************************************************************************
ok: [localhost] => (item={u'repo': {u'repo': u'ppa:ansible/ansible'}, u'name': u'ansible', u'key': {u'keyring': u'/etc/apt/trusted.gpg.d/ansible_ubuntu_ansible.gpg', u'id': u'6125E2A8C77F2818FB7BD15B93C4A3FD7BB9C367', u'keyserver': u'keyserver.ubuntu.com'}}) => {             
    "msg": {
        "key": {
            "id": "6125E2A8C77F2818FB7BD15B93C4A3FD7BB9C367", 
            "keyring": "/etc/apt/trusted.gpg.d/ansible_ubuntu_ansible.gpg", 
            "keyserver": "keyserver.ubuntu.com"
        }, 
        "name": "ansible", 
        "repo": {
            "repo": "ppa:ansible/ansible"
        }
    }
}
ok: [localhost] => (item={u'name': u'UbuntuArchiveSigning', u'key': {u'id': u'790BC7277767219C42C86F933B4FE6ACC0B21F32'}}) => {
    "msg": {
        "key": {
            "id": "790BC7277767219C42C86F933B4FE6ACC0B21F32"
        }, 
        "name": "UbuntuArchiveSigning"
    }
}
ok: [localhost] => (item={u'name': u'UbuntuCDImage', u'key': {u'id': u'843938DF228D22F7B3742BC0D94AA3F0EFE21092'}}) => {
    "msg": {
        "key": {
            "id": "843938DF228D22F7B3742BC0D94AA3F0EFE21092"
        }, 
        "name": "UbuntuCDImage"
    }
}

PLAY RECAP *******************************************************************************************************************************
localhost                  : ok=2    changed=0    unreachable=0    failed=0   
```
